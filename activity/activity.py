from abc import ABC, abstractmethod

class Animals(ABC):
    @abstractmethod
    def eat(self, food):
        pass

    @abstractmethod
    def make_sound(self):
        pass

class Cat(Animals):
    def __init__(self, name, breed, age):
        self._name = name
        self._breed = breed
        self._age = age

    def get_name(self):
        return self._name
    def set_name(self,name):
        self._name = name
    def get_breed(self):
        return self._breed
    def set_breed(self,breed):
        self._breed = breed
    def get_age(self):
        return self._age
    def set_age(self,age):
        self._age = age
    def eat(self, food):
        print(f"{self._name} the Cat is eating {food}")
    def make_sound(self):
        print(f"{self._name} the Cat says meow")
    def call(self):
        print(f"{self._name} is coming")

class Dog(Animals):
    def __init__(self, name, breed, age):
        self._name = name
        self._breed = breed
        self._age = age

    def get_name(self):
        return self._name
    def set_name(self,name):
        self._name = name
    def get_breed(self):
        return self._breed
    def set_breed(self,breed):
        self._breed = breed
    def get_age(self):
        return self._age
    def set_age(self,age):
        self._age = age
    def eat(self, food):
        print(f"{self._name} the Dog is eating {food}")
    def make_sound(self):
        print(f"{self._name} the Dog says bark")
    def call(self):
        print(f"{self._name} is coming")

cat = Cat("Choco", "Egypthian", 5)
cat.eat("labahita")
cat.make_sound()
cat.call()
dog = Dog("Izume", "Shihtzue", 2)
dog.eat("peanut butter")
dog.make_sound()
dog.call()