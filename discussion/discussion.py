## OOP 
## Encapsulation, Inheritance, Abstraction Polymorphism 


## Encapsulation
class Person():
    def __init__(self) : 
        self._name = "John Doe"
        self._age = 0   
    ## Setter methods
    def set_name(self, name):
        self._name = name

    ## Getter method 
    def get_name(self):
        print(f"name of person : {self._name}")

    def get_age(self):
        print(f"age of person : {self._age}")

person1 = Person()
person1.get_name()
person1.set_name("jherson dignadice")
person1.get_age()
person1.get_name()

# [Inheritance]
class Employee(Person):
    def __init__(self, employeeId):
        super().__init__() # gives access to the properties of the parent class (which is the Person class)
        self._employeeId = employeeId

    def get_employeeId(self):
        print(f"The employee ID is {self._employeeId}")
    
    def set_employeeId(self):
        self._employeeId = employeeId

    def get_details(self):
        print(f"{self._employeeId} belongs to {self.name}")

class TeamLead():
    def occupation(self):
        print("Team Lead")
    def hasAuth(self):
        print(True)

class TeamMember():
    def occupation(self):
        print("Team Member")
    def hasAuth(self):
        print(False)

team_lead = TeamLead()
team_member = TeamMember()

for person in (team_lead, team_member):
    person.occupation()
    person.hasAuth()

# [Abstraction]
# Abstraction is when you import class or a function into your current file and are able to use their functionalities without the actual code of those classes/functions actually being in your current file.
from abc import ABC, abstractclassmethod

class Polygon(ABC):
    @abstractclassmethod
    def printNumberOfSides(self):
        # the pass keyword denotes that the method does not do anything
        pass

class Triangle(Polygon):
    def __init__(self):
        super().__init__()
            
    def printNumberOfSides(self):
        print(f"This polygon has 3 sides")
    
shape = Triangle()
shape.printNumberOfSides()